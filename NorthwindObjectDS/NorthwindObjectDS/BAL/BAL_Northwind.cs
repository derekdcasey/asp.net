﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NorthwindObjectDS.Data;

namespace NorthwindObjectDS.BAL
{
    public class BAL_Northwind
    {
        //returns column names from customer table
        public List<string> GetColumns()
        {
            using (var context = new northwindDataDataContext())
            {
                List<string> myList = context.ExecuteQuery<string>
                ("SELECT name FROM sys.columns WHERE object_id = OBJECT_ID('Customers');").ToList();
                return myList;
            }

        }

        public List<Customer> GetAllCustomers(string ddl, string search)
        {
            using (var context = new northwindDataDataContext())
            {
                //FIXME: not complete
                List<Customer> allCustomers = (context.ExecuteQuery<Customer>
                ("SELECT * FROM[Customers] WHERE[" + ddl + "] LIKE '%'" + search + "'%'")).ToList<Customer>();
                return allCustomers;
            }
        }

        public Customer GetCustomer(string id)
        {
            using (var context = new northwindDataDataContext())
            {
                Customer customer = (from data in context.Customers where data.CustomerID == id select data).SingleOrDefault();
                return customer;
            }
        }

    }
}