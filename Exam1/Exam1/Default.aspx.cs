﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Exam1
{
    public partial class StudentInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
            string studentInfo = tbName.Text + " " + tbEmail1.Text + " " + tbDob.Text + " " + ddlClass.SelectedValue;
            lbStudentInfo.Items.Add(studentInfo);
            lbGrades.Items.Add(tbFinalGrade.Text);
            lbAverage.Text = CalcAverage().ToString();
            }
            else {
                return;
            }

            
        }

        public double CalcAverage()
        {
            int count = lbGrades.Items.Count;
            double total = 0;
            double average = 0;

            if (lbGrades.Items.Count > 0) {
            foreach (var grade in lbGrades.Items)
            {
                total += double.Parse(grade.ToString());
               average = total / count;
            }
            return average;
            }
            return 0.0;

        }
    }
}