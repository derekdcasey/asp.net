﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Exam1.Data;

namespace Exam1.BAL
{
    public class BAL_Northwind
    {

        public List<Order> GetAllOrders(DateTime orderDate)
        {
            using (var context = new NorthwindDataContext())
            {
                List<Order> allOrders = (from data in context.Orders where data.OrderDate == orderDate select data).ToList();
                return allOrders;
            }
        }

        public Customer GetCustomer(string id)
        {
            using (var context = new NorthwindDataContext())
            {
                Customer c = (from data in context.Customers where data.CustomerID == id select data).SingleOrDefault();
                return c;
            }
        }


    }
}