﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Exam1.StudentInfo" %>

<%@ Register src="Controls/header.ascx" tagname="header" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            width: 197px;
        }
        .auto-style6 {
            width: 156px;
            height: 23px;
        }
        .auto-style7 {
            width: 185px;
            height: 76px;
            color: #FF0000;
        }
        .auto-style8 {
            width: 156px;
            height: 76px;
        }
        .auto-style9 {
            height: 76px;
        }
        .auto-style10 {
            width: 183px;
        }
        .auto-style11 {
            width: 183px;
            height: 81px;
        }
        .auto-style12 {
            height: 81px;
            width: 153px;
        }
        .auto-style13 {
            width: 791px;
        }
        .auto-style14 {
            height: 81px;
            width: 791px;
        }
        .auto-style15 {
            width: 183px;
            height: 38px;
        }
        .auto-style16 {
            width: 791px;
            height: 38px;
        }
        .auto-style17 {
            height: 38px;
            width: 153px;
        }
        .auto-style18 {
            width: 1395px;
        }
        .auto-style19 {
            width: 1580px;
            height: 270px;
        }
        .auto-style20 {
            width: 1265px;
            height: 195px;
        }
        .auto-style21 {
            width: 197px;
            height: 23px;
        }
        .auto-style22 {
            width: 197px;
            height: 76px;
            text-align: center;
        }
        .auto-style23 {
            width: 98px;
            height: 76px;
            color: #FF0000;
        }
        .auto-style24 {
            width: 98px;
        }
        .auto-style25 {
            width: 98px;
            height: 23px;
        }
        .auto-style26 {
            width: 153px;
        }
    </style>
</head>
<body>
    
    <form id="form1" runat="server">
        <div style="margin:0 auto; width:100%; overflow:auto;">
        <table class="auto-style19">
            <tr>
                <td class="auto-style18">
                    <uc1:header ID="header1" runat="server" />
                </td>
            </tr>
        </table>
            </div>
        <div style="margin:0 auto; width:50%;">
        <table class="auto-style20">
            <tr>
                <td class="auto-style24">Student Name:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="tbName" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbName" ErrorMessage="Is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style24">Email Address:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="tbEmail1" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbEmail1" ErrorMessage="Not a valid email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tbEmail1" ErrorMessage="Is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style24">Confirm Email:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="tbEmail2" runat="server"></asp:TextBox>
                </td>
                <td>
                     <asp:CompareValidator ID="CompareValidatorEmail" runat="server" 
     ControlToValidate="tbEmail2"
     CssClass="ValidationError"
     ControlToCompare="tbEmail1"
     ErrorMessage="No Match" 
     ToolTip="Email must be the same" ForeColor="Red" >*</asp:CompareValidator>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="tbEmail2" ErrorMessage="Is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style24">Date Of Birth:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="tbDob" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="tbDob" ErrorMessage="Not a valid date" ForeColor="Red" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tbDob" ErrorMessage="Is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style24">Class:</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="ddlClass" runat="server" AppendDataBoundItems="True" Height="16px" Width="123px">
                        <asp:ListItem Value="" Text="Select Class"></asp:ListItem>
                        <asp:ListItem>Freshman</asp:ListItem>
                        <asp:ListItem>Sophomore</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlClass" ErrorMessage="Is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style25">Final Grade</td>
                <td class="auto-style21">
                    <asp:TextBox ID="tbFinalGrade" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style4">
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="tbFinalGrade" ErrorMessage="Must be between 0 &amp; 100" ForeColor="Red" MaximumValue="100" MinimumValue="0" Type="Double">*</asp:RangeValidator>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="tbFinalGrade" ErrorMessage="Is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style23">Alerts</td>
                <td class="auto-style22"></td>
                <td class="auto-style9">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
                </td>
            </tr>
        </table>
        <div>
        </div>
        <table class="auto-style1">
            <tr>
                <td class="auto-style10">&nbsp;</td>
                <td class="auto-style13">Student Information</td>
                <td class="auto-style26">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style11">Student Details</td>
                <td class="auto-style14">
                    <asp:ListBox ID="lbStudentInfo" runat="server" Width="530px"></asp:ListBox>
                </td>
                <td class="auto-style12">
                    <asp:ListBox ID="lbGrades" runat="server" Width="85px"></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style15"></td>
                <td class="auto-style16">
                    <asp:Button ID="btnAdd" runat="server" Text="Add student &amp; calculate average" Width="275px" OnClick="btnAdd_Click" />
                </td>
                <td class="auto-style17"></td>
            </tr>
            <tr>
                <td class="auto-style10">Class Average<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                </td>
                <td class="auto-style13">
                    <asp:Label ID="lbAverage" runat="server" Text="0.0"></asp:Label>
                </td>
                <td class="auto-style26">&nbsp;</td>
            </tr>
        </table>
            </div>
    </form>
        
</body>
</html>
