﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EFTimeTracker.Models
{
    public class TimeTrackerRepository
    {
        TimeTracker_DbContext _context = new TimeTracker_DbContext();

        public List<Employee> GetAllEmployees() {
            List<Employee> allEmps = (from data in _context.Employees select data).ToList();
            return allEmps;
        }

        public Employee GetEmployee(int id)
        {
            Employee tempEmp = (from data in _context.Employees where data.Id == id select data).SingleOrDefault();

            return tempEmp;
        }

        public List<TimeCard> GetAllTimeCards(int selectedid)
        {
            List<TimeCard> allTimes =(from data in _context.Employees where data.Id == selectedid select data.TimeCards).SingleOrDefault();
            return allTimes;
        }

        public List<string> GetDepartments()
        {
            List<string> allDpmts = (from data in _context.Employees select data.Department).Distinct().ToList();
            return allDpmts;
        }

        public List<Employee> AllEmpsByDept(string department)
        {
            List<Employee> allEmps = (from data in _context.Employees where data.Department == department select data).ToList();
            return allEmps;
        }
      public void AddEmployee(int id,string fn, string ln, string dept,DateTime date)
        {
            Employee emp = new Employee();
            emp.Id = id;
            emp.FirstName = fn;
            emp.LastName = ln;
            emp.Department = dept;
            emp.HireDate = date;
        
            _context.Employees.Add(emp);
            _context.SaveChanges();
        }


    }
}