﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace EFTimeTracker.Models
{

    //only for test phase
    public class TimeTrackerDbContextInitializer: DropCreateDatabaseIfModelChanges<TimeTracker_DbContext>
    {
        protected override void Seed(TimeTracker_DbContext context)
        {
            Employee tempEmployee = new Employee();
            tempEmployee.FirstName = "Barry";
            tempEmployee.LastName = "Allen";
            tempEmployee.Department = "Forensics";
            tempEmployee.HireDate = DateTime.Now.AddDays(-14);

            List<TimeCard> timecards = new List<TimeCard>
            {
                new TimeCard{SubmissionDate = DateTime.Now, MondayHours =8, TuesdayHours =9, WednesdayHours=6,
                ThursdayHours=8, FridayHours=10, SaturdayHours=0,SundayHours=0 },

                 new TimeCard{SubmissionDate = DateTime.Now.AddDays(-7), MondayHours = 10, TuesdayHours = 7, WednesdayHours=6,
                ThursdayHours=8, FridayHours=10, SaturdayHours=0,SundayHours=3}
            };

            tempEmployee.TimeCards = timecards;
            context.Employees.Add(tempEmployee);
            base.Seed(context);
        }
    }
}