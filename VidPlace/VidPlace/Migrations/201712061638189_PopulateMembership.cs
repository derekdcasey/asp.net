namespace VidPlace.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMembership : DbMigration
    {
        public override void Up()
        {
            Sql("Insert INTO Memberships (Id, SignupFee,DurationInMonths,DiscountRate) VALUES(1,0,0,0)");
            Sql("Insert INTO Memberships (Id, SignupFee,DurationInMonths,DiscountRate) VALUES(2,10,1,5)");
            Sql("Insert INTO Memberships (Id, SignupFee,DurationInMonths,DiscountRate) VALUES(3,30,3,10)");
            Sql("Insert INTO Memberships (Id, SignupFee,DurationInMonths,DiscountRate) VALUES(4,100,12,20)");

        }
        
        public override void Down()
        {
        }
    }
}
