﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace VidPlace.Models
{
    
        public class Customer
        {
        public int Id { get; set; }
        [Required][MaxLength(100)]
        public string Name { get; set; }
            public string Address { get; set; }
        public bool IsSubScribedtoNewsLetter { get; set; }
        public DateTime BirthDate { get; set; }
        public Membership MembershipType { get; set; }
        
        public byte MembershipId { get; set; }

        public override string ToString()
            {
                return "Customer Name: " + Name + ", Address: " + Address;
            }


     

    }

       
    }
