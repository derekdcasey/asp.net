﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VidPlace.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        public string GetString() {
            return "hello world, MVC";
        }

      

        public ActionResult GetView()
        {
            return View("MyView");
        }

        public ActionResult GetDeveloper()
        {
            return View("Developer");
        }

    }
}