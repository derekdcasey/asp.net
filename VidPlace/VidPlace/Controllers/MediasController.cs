﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VidPlace.Models;
using VidPlace.ViewModels;

namespace VidPlace.Controllers
{
    public class MediasController : Controller
    {
        // GET: Medias
        public ActionResult Random()
        {
            Media media = new Media();
            media.Name = "Jurassic Park";
            media.Id = 123;
            return View(media);
        }

        public ActionResult ActionResultDemo()
        {
            //url will be Medias/ActionResultDemo
            //return Content("Hello World");
            //return HttpNotFound();
            //return new EmptyResult();
            //return RedirectToAction("GetCustomer","test");

            //action with query string ?userid=123&sortBy=name
            return RedirectToAction("Index", "Home", new { userId = "123", sortBy = "name" });

        }

        //with parameters
        [Route("medias/edit/{id:int}")]
        public ActionResult edit(int? id)
        {
            //url will be medias/edit/{id}

            return Content("Provide Id " + id);

        }
        [Route("medias/index/{pageIndex:int}/{sortBy:minlength(2)}")]
        public ActionResult Index(int? pageIndex, string sortBy)
        {   //edit routeConfig.cs
            //url will be medias/index/{pageIndex}/{sortBy} 
            //or medias/index?pageIndex=1&sortBy=Date
            //to make an int parameter optionall add a question mark

            if (!pageIndex.HasValue)
                pageIndex = 1;
            if (string.IsNullOrWhiteSpace(sortBy))
                sortBy = "Name";
            return Content("Page Number: " + pageIndex + "Sort By: " + sortBy);

        }

        [Route("medias/released/{year:range(1900,2018)}/{month:range(1,12)}")]
        public ActionResult released(int? year, int? month)
        {
            //url will be medias/released/{year}/month
            if (!year.HasValue)
                year = DateTime.Now.Year;
            if (!month.HasValue)
                month = DateTime.Now.Month;
            return Content("date: " + year + "/" + month);

        }

        [Route("medias/listCustomers")]
        public ActionResult listCustomers()
        {
            Media tempMedia = new Media() { Name = "Pulp Fiction" };
            var tempCustomers = new List<Customer>
            {
                new Customer(){Name="Barry Allen"},
                 new Customer(){Name="Bruce Wayne"},
                  new Customer(){Name="Diana Prince"}
            };


            var viewModelObject = new CustomerMediaViewModel()
            {
                media = tempMedia,
                customersList = tempCustomers

            };
            return View(viewModelObject);
        }


    }
}