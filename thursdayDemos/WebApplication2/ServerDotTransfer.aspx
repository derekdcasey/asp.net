﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServerDotTransfer.aspx.cs" Inherits="WebApplication2.ServerDotTransfer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            margin-left: 11px;
        }
        .auto-style2 {
            margin-left: 9px;
        }
        .auto-style3 {
            margin-left: 53px;
        }
        .auto-style4 {
            margin-left: 52px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Name:<asp:TextBox ID="tbName" runat="server" CssClass="auto-style1"></asp:TextBox>
            <br />
            <br />
            Email:&nbsp;
            <asp:TextBox ID="tbEmail" runat="server" CssClass="auto-style2"></asp:TextBox>
            <br />
        </div>
        <asp:Button ID="btnTransfer" runat="server" CssClass="auto-style3" Height="40px" OnClick="btnTransfer_Click" Text="Server Transfer" Width="124px" />
        <p>
            <asp:Button ID="btnExecute" runat="server" CssClass="auto-style3" Height="40px" OnClick="btnExecute_Click" Text="Server Execute" Width="122px" />
        </p>
        <asp:Label ID="lblOutcome" runat="server" ForeColor="Green"></asp:Label>
        <p>
            <asp:Button ID="btnCross" runat="server" CssClass="auto-style4" Height="42px" Text="Cross Page Posting" Width="123px" />
        </p>
    </form>
</body>
</html>
