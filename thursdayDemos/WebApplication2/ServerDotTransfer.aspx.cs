﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class ServerDotTransfer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnTransfer_Click(object sender, EventArgs e)
        {
            //does not diplay URL on loaded page
            Server.Transfer("~/Webform1.aspx");
            
        }

        protected void btnExecute_Click(object sender, EventArgs e)
        {
            Server.Execute("~/Webform1.aspx");
            lblOutcome.Text = "YOU HAVE DONE IT!";
        }
    }
}