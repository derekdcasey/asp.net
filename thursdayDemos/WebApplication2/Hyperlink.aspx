﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Hyperlink.aspx.cs" Inherits="WebApplication2.Hyperlink" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:HyperLink ID="myHL" NavigateUrl="~/default.aspx" runat="server">HyperLink</asp:HyperLink>
            <br />
            <br />
        </div>
        <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="True" AutoPostBack="True" Height="28px" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" Width="230px">
            <asp:ListItem Value="" Text="Select One"></asp:ListItem>
            <asp:ListItem>default</asp:ListItem>
            <asp:ListItem>default</asp:ListItem>
            <asp:ListItem>SuperHeroHype</asp:ListItem>
        </asp:DropDownList>
    </form>
</body>
</html>
