﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace WebApplication2
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            Application["TotalApplications"] = 0;
            Application["TotalSessions"] = 0;

            Application["TotalApplications"] = (int)Application["TotalApplications"] + 1;
        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

            Application["TotalSessions"] = (int)Application["TotalSessions"] + 1;

        }


    }
}