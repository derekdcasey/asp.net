﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // use namespace ^^
            //Method 1
            //NameValueCollection previousCollection = Request.Form;

            //lbName.Text = previousCollection["tbName"];
            //lbEmail.Text = previousCollection["tbEmail"];

            //method 2
            Page previousPage = Page.PreviousPage;
            if(previousPage != null)
            {
            lbName.Text =((TextBox)previousPage.FindControl("tbName")).Text;
            lbEmail.Text = ((TextBox)previousPage.FindControl("tbEmail")).Text;
            }
        }
    }
}
