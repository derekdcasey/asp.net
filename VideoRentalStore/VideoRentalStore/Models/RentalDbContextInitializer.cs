﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using VideoRentalStore.Models;

namespace VideoRentalStore.Models
{
    public class RentalDbContextInitializer: DropCreateDatabaseIfModelChanges<RentalDbContext>
    {
        protected override void Seed(RentalDbContext context)
        {
            Customer tempCust = new Customer();
            tempCust.FirstName = "Barry";
            tempCust.LastName = "Allen";
            tempCust.Address = "123 street";
            tempCust.PhoneNumber = "514-333-4444";
            List<Media> mediaList = new List<Media> {
                new Media{Title="Blade Runner", Type="Movie", Year= DateTime.Now.Year}
            };
            List<Rental> rentalList = new List<Rental>
            {


                new Rental{RentalDate = DateTime.Now, RentedMedia = mediaList }

            };

            tempCust.RentalRecords = rentalList;
            context.Customers.Add(tempCust);
            base.Seed(context);
        }

    }
}