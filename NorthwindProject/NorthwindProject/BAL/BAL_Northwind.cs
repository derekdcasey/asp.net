﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NorthwindProject.Data; //add namespace

namespace NorthwindProject.BAL
{
    public class BAL_Northwind
    {
        //returns list of distinct countries from customer table
        public List<string> GetCountries() {
            using (var context = new NorthWindDataContext())
            {
                List<string> myList = (from data in context.Customers select data.Country).Distinct().ToList();
                return myList;
            }

        }

        public List<Customer> GetAllCustomers(string country)
        {
            using (var context = new NorthWindDataContext())
            {
                List<Customer> allCustomers = (from data in context.Customers where data.Country == country select data).ToList();
                return allCustomers;
            }
        }

        public Customer GetCustomer(string id)
        {
            using (var context = new NorthWindDataContext())
            {
                Customer customer = (from data in context.Customers where data.CustomerID == id select data).SingleOrDefault();
                return customer;
            }
        }


    }
}