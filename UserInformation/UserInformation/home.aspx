﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="UserInformation.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: 331px;
        }
        .auto-style2 {
            width: 153px;
        }
        .auto-style3 {
            width: 153px;
            height: 23px;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            width: 236px;
        }
        .auto-style6 {
            height: 23px;
            width: 236px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultfocus="tbName">
        <div>
           
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">Name:</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="tbName" runat="server" Width="220px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvName" runat="server" ControlToValidate="tbName" ErrorMessage="Name is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Date of birth:</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="tbDateOfBirth" runat="server" Width="220px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvDOB" runat="server" ControlToValidate="tbDateOfBirth" ErrorMessage="Date of Birth is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvDate" runat="server" ControlToValidate="tbDateOfBirth" ErrorMessage="This is not a valid date" ForeColor="#33CC33" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Email:</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="tbEmail" runat="server" Width="220px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvEmail" runat="server" ControlToValidate="tbEmail" ErrorMessage="Email is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regExEmail" runat="server" ControlToValidate="tbEmail" ErrorMessage="This is not a valid email" ForeColor="#33CC33" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Province</td>
                    <td class="auto-style5">
                        <asp:DropDownList ID="ddlProvince" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlProvince_SelectedIndexChanged" Height="16px" Width="225px">
                            <asp:ListItem Value="">Select Province</asp:ListItem>
                            <asp:ListItem>QC</asp:ListItem>
                            <asp:ListItem>ON</asp:ListItem>
                            <asp:ListItem>BC</asp:ListItem>
                            <asp:ListItem>AL</asp:ListItem>
                            <asp:ListItem>Other</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvProvince" runat="server" ControlToValidate="ddlProvince" ErrorMessage="Province is Required" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">City:</td>
                    <td class="auto-style5">
                        <asp:DropDownList ID="ddlCity" runat="server" Height="28px" Width="225px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">Count:<input type="hidden" id="hiddenCount" value="0" runat="server"/></td>
                    <td class="auto-style5">
                        <asp:Label ID="lblCount" runat="server"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">Result</td>
                    <td class="auto-style6">
                        <asp:Label ID="lblResult" runat="server"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        &nbsp;</td>
                </tr>
                 <tr>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style6">
                       
                        <asp:Button ID="btnSubmit" runat="server" BackColor="#00CC99" Text="Submit" Width="225px" BorderStyle="None" Height="54px" OnClick="btnSubmit_Click" Font-Bold="True" ForeColor="White" />
                       
                    </td>
                    <td class="auto-style4">
                       
                        &nbsp;</td>
                </tr>
                 <tr>
                    <td class="auto-style4" colspan="3">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
                     </td>
                </tr>
                 <tr>
                    <td class="auto-style3">
                        List Of Users:</td>
                    <td class="auto-style6">
                       
                        <asp:ListBox ID="lbUsers" runat="server" Width="225px"></asp:ListBox>
                       
                    </td>
                    <td class="auto-style4">
                       
                        &nbsp;</td>
                </tr>
            </table>
           
        </div>
    </form>
</body>
</html>
