﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UserInformation
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }
        //increment and return current count
        private int CountUpdate()
        {
            int count = int.Parse(hiddenCount.Value);
            count++;
            hiddenCount.Value = count.ToString();
            return count;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //ensures server validation even if user has scripts disabled
            Page.Validate();
            if (Page.IsValid)
            {
            string newUser = GetData();
            lblResult.Text = newUser;
            lblCount.Text = CountUpdate().ToString();
            lbUsers.Items.Add(newUser);
            ClearForm();
            }
        }

        //this method returns all the data from the form as a string
        private string GetData()
        {
            string name = tbName.Text;
            string dateOfBirth = tbDateOfBirth.Text;
            string email = tbEmail.Text;
            string province = ddlProvince.SelectedValue;
            string city = ddlCity.SelectedValue;

            return (name + ", " + dateOfBirth + ", " + email + ", " + city + ", " + province);
        }
        //change dropdown properties to autoPostBack=true
        //populate cities ddl based on province selection
        protected void ddlProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            string province = ddlProvince.SelectedValue;

            ddlCity.Items.Clear();

            if (province.Equals("QC"))
            {
                ddlCity.Items.Add("Montreal");
                ddlCity.Items.Add("Quebec City");
                ddlCity.Items.Add("Other");
            }
            else if (province.Equals("ON"))
            {
                ddlCity.Items.Add("Toronto");
                ddlCity.Items.Add("Kingston");
                ddlCity.Items.Add("Ottawa");
                ddlCity.Items.Add("Other");
            }
            else if (province.Equals("AL"))
            { 
                ddlCity.Items.Add("Calgary");
                ddlCity.Items.Add("Edmonton");
                ddlCity.Items.Add("Other");
            }
            else if (province.Equals("BC"))
            { 
                ddlCity.Items.Add("Vancouver");
                ddlCity.Items.Add("Victoria");
                ddlCity.Items.Add("Other");
            }
            else if (province.Equals("Other"))
            {    
                ddlCity.Items.Add("Other");
            }
        }
        //reuseable method to clear the form
        private void ClearForm()
        {
            tbName.Text = "";
            tbDateOfBirth.Text = "";
            tbEmail.Text = "";
            ddlProvince.SelectedIndex = 0;
            ddlCity.Items.Clear();
        }
    }
}