﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="CheckoutPage.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 109px;
        }
        .auto-style3 {
            width: 110px;
        }
        .auto-style4 {
            width: 108px;
        }
        .auto-style5 {
            width: 135px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <h1>
            Check Out Page</h1>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" HeaderText="Please correct these entries:" />
        <h2>
            Contact Information</h2>
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Email Address:</td>
                <td>
                    <asp:TextBox ID="tbEmail1" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbEmail1" ErrorMessage="Email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tbEmail1" ErrorMessage="Email is required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Email Confirm:</td>
                <td>
                    <asp:TextBox ID="tbEmail2" runat="server"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="tbEmail1" ControlToValidate="tbEmail2" ErrorMessage="Emails do not match" ForeColor="Red">*</asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tbEmail2" ErrorMessage="Email is required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">First Name:</td>
                <td>
                    <asp:TextBox ID="tbFName" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Last Name:</td>
                <td>
                    <asp:TextBox ID="tbLName" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Phone Number:</td>
                <td>
                    <asp:TextBox ID="tbPhone" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="tbPhone" ErrorMessage="Phone number format" ForeColor="Red" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$">*</asp:RegularExpressionValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <h2>
            &nbsp;Billing Address</h2>
        <table class="auto-style1">
            <tr>
                <td class="auto-style4">Address:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="tbBAddress" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">City:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="tbBCity" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">State:</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="ddlBState" runat="server" Width="125px" AppendDataBoundItems="True">
                        <asp:ListItem Value="" Text="Select One"></asp:ListItem>
                        <asp:ListItem>Qc</asp:ListItem>
                        <asp:ListItem>On</asp:ListItem>
                        <asp:ListItem>Bc</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="State is a required field" ForeColor="Red" ControlToValidate="ddlBState">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Zip Code:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="tbBZip" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <h2>
            Shipping Address</h2>
        <h6>
                    <asp:CheckBox ID="cbSameAddress" runat="server" Text="Same as Billing Address" AutoPostBack="True" OnCheckedChanged="cbSameAddress_CheckedChanged" />
                </h6>

        <table runat="server" id="shipTable" class="auto-style1">
            <tr>
                <td class="auto-style3">Address:</td>
                <td>
                    <asp:TextBox ID="tbSAddress" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">City:</td>
                <td>
                    <asp:TextBox ID="tbSCity" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">State:</td>
                <td>
                    <asp:DropDownList ID="ddlSState" runat="server" Width="123px">
                        <asp:ListItem>Qc</asp:ListItem>
                        <asp:ListItem>On</asp:ListItem>
                        <asp:ListItem>Bc</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Zip code:</td>
                <td>
                    <asp:TextBox ID="tbSZip" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            </table>
        <br />
                    <asp:Button ID="btnCheckout" runat="server" Text="Check Out" Width="123px" OnClick="btnCheckout_Click" />
                <p>
                    <asp:Label ID="lblOutput" runat="server"></asp:Label>
        </p>
    </form>
</body>
</html>
