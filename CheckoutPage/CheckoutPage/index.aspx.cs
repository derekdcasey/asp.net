﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CheckoutPage
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }

        protected void cbSameAddress_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSameAddress.Checked)
            {
                tbSAddress.Enabled = false;
                tbSCity.Enabled = false;
                tbSZip.Enabled = false;
                ddlSState.Enabled = false;
                shipTable.Visible = false;
            }
            else
            {
                tbSAddress.Enabled = true;
                tbSCity.Enabled = true;
                tbSZip.Enabled = true;
                ddlSState.Enabled = true;
                shipTable.Visible = true;
            }
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            lblOutput.Text = "Thank you for your order" + tbBZip.Text + "," + tbBCity.Text + "," + tbBAddress.Text + "," + tbEmail1.Text + "," + tbFName.Text + " " + tbLName.Text + "," + ddlBState.Text + "," + tbPhone.Text;
        }
    }
}